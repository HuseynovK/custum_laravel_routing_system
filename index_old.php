<?php require_once "views/layouts/head.php" ?>
<?php
$page = !empty($_GET['page']) ? $_GET['page'] : 'home';
$FullViewPage = 'views/' . $page . ".php";
if (file_exists($FullViewPage)) {
    require_once($FullViewPage);
} else {
    echo "<h1>404 Not Found</h1>";
}
?>
<?php require_once "views/layouts/footer.php" ?>
