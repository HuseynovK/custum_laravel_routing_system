<?php
require_once('app/web/config.php');

//echo '<pre>';
//echo $_SERVER['REQUEST_URI'];
$path = $_SERVER['REQUEST_URI'];
$params = explode("/", $path);
print_r($params);
if (array_key_exists($params[1], $routes)) {
    $controllerName = $routes[$params[1]];
    $controller = new $controllerName();
    $method = 'index';
    if (!empty($params[2])) {
        $method = $params[2];
    }
    if (method_exists($controller, $method)) {
        $controller->{$method}();
    } else {
        echo "<h1>404 Not Found</h1>";
    }
//    $controller->index();
//    echo $controller;
} else {
    echo "<h1>Error 404 not found</h1>";
}
