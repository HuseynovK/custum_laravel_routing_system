<?php

spl_autoload_register(function ($className) {
    require_once($className . ".php");
});
$root = $_SERVER['DOCUMENT_ROOT'];
require_once($root . '/app/helpers/ViewHelper.php');
require_once($root . '/app/helpers/UrlHelper.php');
require_once($root . '/app/web/routes.php');


