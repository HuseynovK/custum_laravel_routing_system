<?php

namespace app\web;

use PDO;

class db
{
    public static function connect()
    {
        $database = ["host" => "localhost",
            "charset" => "utf8mb4_general_ci",
            "username" => "root",
            "password" => "",
            "dbname" => "jugaad"];

        $connect = null;

        $connect = new PDO("mysql:host=localhost;dbname=jugaad;charset=utf8", $database['username'], $database['password']);
        return $connect;
    }
}