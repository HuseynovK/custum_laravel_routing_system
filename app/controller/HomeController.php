<?php

namespace app\controller;

use app\model\Jugaad;

class HomeController
{
    public function index()
    {
//        echo $_SERVER['DOCUMENT_ROOT'];
//        $root = $_SERVER['DOCUMENT_ROOT'] . "\\views\home.php";
//        require_once($root);
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'home'
        ];
        view('layouts/head', $data);
    }

    public function add_contact_message()
    {
//        var_dump($_POST);
//        die();
        $data = [
            "page" => 'contact',
            "error" => 0
        ];
//        var_dump(Jugaad::InsertMessage(["email" => $_POST['email'], "message" => $_POST['message']]));
//        die();
        if (Jugaad::InsertMessage(["email" => $_POST['email'], "message" => $_POST['message']])) {
            view('layouts/head', $data);
        } else {
            $data['error'] = 1;
            view('layouts/head', $data);
        }
    }

    public function about()
    {
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'about'
        ];
        view('layouts/head', $data);
    }

    public function contact()
    {
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'contact'
        ];
        view('layouts/head', $data);
    }

    public function services()
    {
        $data = [
            "name" => ['Kamil', 'Huseynov'],
            "page" => 'services'
        ];
    }
}
