<?php

namespace app\controller;

class ProductController
{
    public function index()
    {
        echo "<h1>Home Page</h1>";
    }

    public function about()
    {
        echo "<h1>About Page</h1>";
    }

    public function contact()
    {
        echo "<h1>Contact Page</h1>";
    }

    public function services()
    {
        echo "<h1>Services Page</h1>";
    }
}
